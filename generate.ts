import * as fs from 'fs';
import { compileFromFile } from 'json-schema-to-typescript';

async function generate() {
  fs.writeFileSync('./generated/resume.d.ts', await compileFromFile('./schema/schema.json'));
}

generate().then(() => console.log('Done!'));
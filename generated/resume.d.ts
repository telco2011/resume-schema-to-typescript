export type Name = string;
/**
 * Job position information.
 */
export type JobPosition = "Senior Software Architect" | "Junior Software Architect" | "Software Architect" | "Full Stack Developer";
export type Enterprise = string;
/**
 * Type of the phone.
 */
export type Type = "iPhone" | "mobile" | "home";
export type Mail = string;
/**
 * Principal email to contact.
 */
export interface Email {
  /**
   * Type of the mail.
   */
  type: Type;
  /**
   * Mail.
   */
  mail: Mail;
  [k: string]: any;
}
export type OtherMails = Email[];
/**
 * Email information data.
 */
export interface EmailInformation {
  /**
   * Principal email to contact.
   */
  principalEmail: Email;
  /**
   * Other mail to contact.
   */
  otherMails?: OtherMails;
  [k: string]: any;
}
export type PhoneType = string;
/**
 * Principal phone to contact.
 */
export interface Phone {
  /**
   * Type of the phone.
   */
  type: Type;
  /**
   * Phone number.
   */
  phone: PhoneType;
  [k: string]: any;
}
export type OtherPhones = PhoneType[];
/**
 * Phone information.
 */
export interface PhoneInformationSchema {
  /**
   * Principal phone to contact.
   */
  principalPhone: PhoneType;
  /**
   * Others phone to contact.
   */
  otherPhones?: OtherPhones;
  [k: string]: any;
}
export type SocialInformation = [{"$ref":"#/definitions/social"}][];
/**
 * Degree information.
 */
export type Degree = "Engineer's Degree" | "MSc" | "BSc";
export type FieldOfStudy = string;
export type School = string;
export type Year = number;
/**
 * Month information.
 */
export type Month = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
/**
 * Day information.
 */
export type Day = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31;
export interface Date {
  /**
   * Year information.
   */
  year: Year;
  /**
   * Month information.
   */
  month?: Month;
  /**
   * Day information.
   */
  day?: Day;
  [k: string]: any;
}
export type Present = boolean;
/**
 * Period information.
 */
export interface Period {
  startDate: Date;
  endDate?: Date;
  /**
   * If is present date.
   */
  present?: Present;
  [k: string]: any;
}

export type Education = {
  /**
   * Degree information.
   */
  degree?: Degree;
  /**
   * Field of study information.
   */
  FieldOfStudy?: FieldOfStudy;
  /**
   * School name information.
   */
  school?: School;
  /**
   * Period information.
   */
  period?: Period;
  [k: string]: any;
}[];
export type LanguagesSchema = [{"$ref":"#/definitions/language"}][];
export type Interests = [{"type":"string","description":"Interest tag."}][];
/**
 * Provided's personal information.
 */
export interface PersonalInfo {
  picture?: any;
  /**
   * Name information.
   */
  name: Name;
  /**
   * Job position information.
   */
  jobPosition: JobPosition;
  /**
   * Enterprise name.
   */
  enterprise: Enterprise;
  /**
   * Email information data.
   */
  emailInformation: EmailInformation;
  /**
   * Phone information.
   */
  phoneInformation: PhoneInformationSchema;
  /**
   * Social network information.
   */
  socialInformation: SocialInformation;
  /**
   * Education information data.
   */
  education: Education;
  /**
   * An explanation about the purpose of this instance.
   */
  languages: LanguagesSchema;
  /**
   * Interest information.
   */
  interests: Interests;
  [k: string]: any;
}
export type CareerProfile = string;
export type JobTitle = string;
export type Location = string;
export type Description = string;
/**
 * Experience information.
 */
export interface Experience {
  /**
   * Job title name.
   */
  jobTitle: JobTitle;
  /**
   * Enterprise name.
   */
  enterprise: Enterprise;
  /**
   * Location information.
   */
  location?: Location;
  /**
   * Period information.
   */
  period: Period;
  /**
   * Bravely description of the experience.
   */
  description: Description;
  [k: string]: any;
}

export type ExperienciesSchema = Experience[];
export type SkillsSchema = [{"$ref":"#/definitions/technology"}][];
export type Projects = any[];
/**
 * Provide profile data information.
 */
export interface ProfileSchema {
  /**
   * Provided's personal information.
   */
  personalInfo: PersonalInfo;
  /**
   * Career profile summary information.
   */
  careerProfile: CareerProfile;
  /**
   * An explanation about the purpose of this instance.
   */
  experiencies: ExperienciesSchema;
  /**
   * An explanation about the purpose of this instance.
   */
  skills: SkillsSchema;
  /**
   * Projects information.
   */
  projects?: Projects;
  [k: string]: any;
}